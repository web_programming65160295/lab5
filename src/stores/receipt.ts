import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import { watch } from 'vue'

export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMemberStore()
  const receiptDialog = ref(false)
  const receipt = ref<Receipt>({
    id: 0,
    createdDate: new Date(),
    totalBefore: 0,
    memberDiscount: 0,
    total: 0,
    receivedAmount: 0,
    change: 0,
    PaymentType: '',
    userId: authStore.currentUser.id,
    user: authStore.currentUser,
    memberId: 0,
  })
  const receiptItems = ref<ReceiptItem[]>([])
  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.products?.id === product.id)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    }
    else {
      const newReceiptItem: ReceiptItem = {
        id: -1,
        name: product.name,
        price: product.price,
        unit: 1,
        productsId: product.id,
        products: product
      }
      receiptItems.value.push(newReceiptItem)
      calReceipt()
    }

  }

  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit == 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }

  function calReceipt() {
    let totalBefore = 0
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + (item.price * item.unit)
    }

    receipt.value.totalBefore = totalBefore
    if (memberStore.currentMember) {
      receipt.value.total = receipt.value.totalBefore * 0.95
    }
    else {
      receipt.value.total = receipt.value.totalBefore

    }


  }
  function showReceiptDialog() {
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true

  }
  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      totalBefore: 0,
      memberDiscount: 0,
      total: 0,
      receivedAmount: 0,
      change: 0,
      PaymentType: 'cash',
      userId: authStore.currentUser.id,
      user: authStore.currentUser,
      memberId: 0,
    }
    memberStore.clear()
  }

  return {
    dec, inc, removeReceiptItem, addReceiptItem, calReceipt, showReceiptDialog, clear,
    receiptDialog, receiptItems, receipt
  }
})
